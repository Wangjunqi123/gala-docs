# 介绍

![](./png/logo.png)

gala是一款C/S架构、基于AI的操作系统亚健康诊断工具。其基于eBPF + java agent无侵入观测技术，并以AI技术辅助，实现亚健康故障（比如性能抖动、错误率提升、系统卡顿等问题现象）分钟级诊断，简化IT基础设施的运维过程。

# 背景

云基础设施在近几年随着云原生、无服务化等技术的实施，其运维的复杂性变得越来越有挑战性，尤其是亚健康问题特点（间歇性出现、持续时间短、问题种类多、涉及范围广等）给云基础设施故障诊断带来重要挑战。亚健康故障诊断的挑战（包括可观测能力、海量数据管理能力、AI算法的泛化能力等）在Linux场景中变的尤为突出。在openEuler开源操作系统中，现有的运维手段不足以及时发现、定位亚健康问题，存在包括：缺乏在线、持续性监控能力；缺乏应用视角精细化的观测能力；缺乏基于全栈观测数据的自动化、AI分析能力等问题。然而，针对亚健康故障的诊断能力其难点包括：

- 全栈的无侵入可观测观测能力。
- 持续、精细化、低负载的监控能力。
- 自适应不同应用场景的异常检测、可视化故障推导能力。

# 项目简介

gala的整体架构如图所示，其整体上是一个C/S架构。在生产节点gala-gopher是一个Linux后台程序，其负责提供全场景、全栈（包括Metrics、Events、Tracing等）的数据采集，其支持通过OpenTelemetry开放生态接口（支持prometheus exporter、kafka client等）将数据传递给管理节点。管理节点部署gala-spider、gala-anteater组件，分别负责集群拓扑计算、可视化根因推导；

gala架构上依赖一些开源中间件（包括prometheus、kafka、Elastic等），但亦可对接至客户IT系统现有的中间件。gala架构设计提供被集成能力，可以由行业客户IT运维系统集成。其提供两类被集成方式：

- 软件生态集成方式：可以只使用gala-gopher可观测能力（OpenTelemetry方式获取数据），亦可以使用全部能力，通过prometheus、Elastic、kafka等中间件获取观测数据、异常检测结果、可视化推导结果。
- 工具集成方式：将gala提供的能力以Grafana形式集成至客户IT运维系统内。

![](./png/gala-arch.png)



gala可以给客户提供如下运维能力：

- 在线应用性能抖动诊断：提供数据库类应用性能在线诊断能力，包括网络类（丢包、重传、时延、TCP零窗等）问题、I/O类（磁盘慢盘、I/O性能下降等）问题，调度类（包括sysCPU冲高、死锁等）问题、内存类（OOM、泄漏等）问题等。
- 系统性能瓶颈诊断：提供通用场景的TCP、I/O性能抖动问题诊断能力。
- 系统隐患巡检：提供内核协议栈丢包、虚拟化网络丢包、TCP异常、I/O时延异常、系统调用异常、资源泄漏、JVM异常、应用RPC异常（包括8种常见协议的错误率、时延等）硬件故障（UCE、磁盘介质错误等）等秒级巡检能力。
- 系统全栈I/O可观测：提供面向分布式存储场景的I/O全栈观测能力，包括GuestOS 进程级、Block层的I/O观测能力，以及虚拟化层存储前端I/O观测能力，分布式存储后端I/O观测能力。
- 精细化性能Profiling：提供多维度（包括系统、进程、容器、Pod等多个维度）、高精度（10ms采样周期）的性能（包括CPU性能、内存占用、资源占用、系统调用等类型）火焰图、时间线图，可实时在线持续性采集。
- K8S Pod全栈可观测及诊断：提供K8S视角的Pod集群业务流实时拓扑能力，Pod性能观测能力、DNS观测能力、SQL观测能力等。

 

gala涉及的关键技术包括如下：

- 融合型非侵入观测技术：融合eBPF、Java agent等不同观测技术优点，实现多语言（支持C/C++、Java、Go等主流语言）、全软件栈（包括内核、系统调用、基础库Glibc、运行时jvm、基础中间件Nginx/Haproxy等）的观测能力。
- 流程拓扑：基于时序化数据（L4/L7层流量等），实时计算生成时序化拓扑结构，动态展现业务集群拓扑变化。
- 可视化根因定位：统计推理模型结合全流程拓扑，实现可视化&分钟级的问题根因诊断。

# 应用场景

​	gala在openEuler等Linux环境主要面向场景包括数据库、分布式存储、虚拟化、云原生等场景。助力金融、电信、互联网等行业客户在全栈可观测的基础上实现亚健康故障分钟级诊断。

# 项目代码仓

https://gitee.com/openeuler/gala-gopher

https://gitee.com/openeuler/gala-spider

https://gitee.com/openeuler/gala-anteater



# 快速安装

## 架构

gala是C/S架构，可以集群方式部署，也可以单机部署。整个架构由[gala-gopher](#gala-gopher)、gala-ops两个软件组成，在集群模式下，gala-gopher安装在生产节点内，gala-ops安装在管理节点内；单机模式两者均安装在生产节点内。

其中，gala-ops软件内包括[gala-spider](#gala-spider)、[gala-anteater](#gala-anteater)、[gala-inference](#gala-inference)组件。

![](./png/csp_arch.png)

<a id="gala-gopher"></a>

## gala-gopher

### 定位

- **数据采集器**：提供应用粒度low-level的数据采集，包括网络、磁盘I/O、调度、内存、安全等方面的系统指标采集，同时负责应用KPI数据的采集。数据类型包括logging、tracing、metrics。
- **系统异常检测**：提供系统异常检测能力，覆盖网络、磁盘I/O、调度、内存等方面的场景系统异常，用户可以通过阈值设置异常上下限范围。
- **性能热点分析**：提供CPU、内存、IO火焰图。

### 原理及术语

gala-gopher软件架构参考[这里](https://gitee.com/openeuler/gala-gopher/tree/master#%E8%BF%90%E8%A1%8C%E6%9E%B6%E6%9E%84)，其是一款基于eBPF技术的低负载探针框架，除了其自身采集的数据外，用户可以自由扩展第三方探针。

**术语**

- **探针**：gala-gopher内执行具体数据采集任务的程序，包括native、extend 两类探针，前者以线程方式单独启动数据采集任务，后者以子进程方式启动数据采集任务。gala-gopher可以通过配置修改的方式启动部分或全部探针。
- **观测实体（entity_name）**：用来定义系统内的观测对象，所有探针采集的数据均会归属到具体的某个观测实体。每种观测实体均有key、label（可选）、metrics组成，比如tcp_link观测实体的key包括进程号、IP五元组、协议族等信息，metrics则包括tx、rx、rtt等运行状态指标。除原生支持的观测实体，gala-gopher也可以扩展观测实体。
- **数据表（table_name）**：观测实体由1张或更多数据表组合而成，通常1张数据表由1个采集任务完成，由此可知单个观测实体可以由多个采集任务共同完成。
- **meta文件**：通过文件定义观测实体（包括内部的数据表），系统内meta文件必须保证唯一，定义不可冲突。规范参考[这里](https://gitee.com/openeuler/gala-gopher/blob/master/doc/how_to_add_probe.md#meta%E6%96%87%E4%BB%B6%E5%AE%9A%E4%B9%89%E8%A7%84%E8%8C%83)。

### 支持的技术

采集范围：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md)。覆盖网络、I/O、内存、网卡、调度、Redis、kafka、Nginx等内核及基础软件的RED（Request、Error、Delay）数据观测。

系统异常范围：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md)。覆盖包括TCP、Socket、进程/线程、I/O、调度等超过60个系统隐患点自动巡检及上报能力。

### 安装及使用

参考[这里](https://gitee.com/openeuler/gala-gopher#%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B)

### 扩展数据采集范围

用户如果希望扩展数据采集范围，只需执行2步：定义观测实体，集成数据探针。

- **定义观测实体**

通过定义观测实体（或者更新原观测实体）用于承载新增采集metrics数据。用户通过meta文件（参考[这里](https://gitee.com/openeuler/gala-gopher/blob/master/doc/how_to_add_probe.md#2-%E5%AE%9A%E4%B9%89meta%E6%96%87%E4%BB%B6)）定义观测实体的key、label（可选）、metrics，定义完成后，将meta文件归档在[探针目录](https://gitee.com/openeuler/gala-gopher/blob/master/doc/how_to_add_probe.md#%E5%BC%80%E5%8F%91%E8%A7%86%E5%9B%BE)。

- **集成数据探针**

用户可以通过各种编程语言（shell、python、java等）包装数据采集软件，并在脚本中按照meta文件定义格式将采集到的数据通过linux管道符形式输出，参考[这里](https://gitee.com/openeuler/gala-gopher/blob/master/doc/how_to_add_probe.md#3-%E8%BE%93%E5%87%BA%E6%8E%A2%E9%92%88%E6%8C%87%E6%A0%87-1)。

参考[cAdvisor第三方探针集成案例](https://gitee.com/openeuler/gala-gopher/blob/master/doc/how_to_add_probe.md#%E5%A6%82%E4%BD%95%E6%96%B0%E5%A2%9Eextends%E6%8E%A2%E9%92%88)。

<a id="gala-spider"></a>

## gala-spider

### 定位

- **拓扑图构建**：提供 OS 级别的拓扑图构建功能，它将定期获取从 gala-gopher 采集的所有观测对象实例的数据，并计算它们之间的拓扑关系，最终将生成的拓扑图保存到图数据库 arangodb 中。

### 原理及术语

参考[这里](https://gitee.com/openeuler/gala-spider/blob/master/docs/devel/zh-CN/spider_design.md)。

### 支持的技术

**支持的拓扑关系类型**

OS 观测实体之间往往存在物理上或逻辑上的关系，比如线程和进程之间具有从属关系，进程和进程之间往往会有连接关系。因此，gala-spider 定义了一些通用的拓扑关系类型，详情参见 gala-spider 设计文档：[关系类型定义](https://gitee.com/openeuler/gala-spider/blob/master/docs/devel/zh-CN/how_to_add_new_observe_object.md#%E5%85%B3%E7%B3%BB%E7%B1%BB%E5%9E%8B%E5%AE%9A%E4%B9%89)。定义好了拓扑关系类型后，接下来就可以定义观测实体之间的拓扑关系，进而构建拓扑图。

**支持的实体关系列表**

gala-spider 默认定义了一些观测实体之间的拓扑关系，这些拓扑关系是可配置和可扩展的，详情参见 gala-spider 设计文档：[支持的拓扑关系](https://gitee.com/openeuler/gala-spider/blob/master/docs/devel/zh-CN/how_to_add_new_observe_object.md#%E6%94%AF%E6%8C%81%E7%9A%84%E6%8B%93%E6%89%91%E5%85%B3%E7%B3%BB)。

### 安装及使用

参考[这里](https://gitee.com/openeuler/gala-spider/blob/master/README.md)。

### 扩展观测实体及关系

参考[这里](https://gitee.com/openeuler/gala-spider/blob/master/docs/devel/zh-CN/how_to_add_new_observe_object.md)。

<a id="gala-anteater"></a>

## gala-anteater

### 定位
* **异常检测**：针对操作系统，提供分钟级别的异常检测能力，能够及时发现潜在影响客户端时延的系统级异常，辅助运维人员，快速跟踪并解决问题。
* **异常上报**：当发现异常行为，平台能够实时上报至Kafka，运维人员只需订阅Kafka消息队列，即可了解当前系统是否潜在风险。


### 原理及术语 <a id="gala-anteater-1"></a>

gala-anteater是一款基于AI的操作系统异常检测平台。主要涵盖时序数据预处理、异常点发现、以及异常上报等功能。基于线下预训练、线上模型的增量学习与模型更新，能够很好地适应于多维多模态数据故障诊断。

* 基本原理

  通过线上线下相结合，利用**在线学习**技术，实现模型的线下学习，线上更新，并应用于线上异常检测。

  **Offline**: 首先，利用线下历史KPI数据集，经过数据预处理、特征选择，得到训练集；然后，利用得到的训练集，对无监督神经网络模型（例如Variational Autoencoder）进行训练调优。最后，利用人工标注的测试集，选择最优模型。

  **Online**: 将线下训练好的模型，部署到线上，然后利用线上真实的数据集，对模型进行在线训练以及参数调优，然后利用训练好的模型，进行线上环境的实时异常检测。

  ![](./png/anteater_arch.png)

### 安装及使用
参考[这里](https://gitee.com/openeuler/gala-anteater/blob/master/README.md)

<a id="gala-inference"></a>

## gala-inference

### 定位

- **根因定位**：提供异常 KPI 的根因定位能力，它基于异常检测的结果和拓扑图作为输入，并将根因定位的结果输出到 kafka 中。

### 原理及术语

参考[这里](https://gitee.com/openeuler/gala-spider/blob/master/docs/devel/zh-CN/infer-design.md)。

### 支持的技术

**专家规则**

为了提升根因定位结果的准确性和可解释性，我们对操作系统领域内观测实体之间实际存在的一些因果关系进行了分析，并总结出一些通用的专家规则，用于指导后续的根因定位算法。这些通用专家规则的详细内容参见 gala-inference 设计文档：[专家规则](https://gitee.com/openeuler/gala-spider/blob/master/docs/devel/zh-CN/infer-design.md#%E4%B8%93%E5%AE%B6%E8%A7%84%E5%88%99)。

### 安装及使用

参考[这里](https://gitee.com/openeuler/gala-spider/blob/master/README.md)。

## gala系统集成

gala还依赖一些开源软件，包括kafka、arangodb、prometheus等。下图介绍gala系统集成关系，kafka用于传输logs/tracing类数据至ES/logstash/jaeger，prometheus用于存储Metrics数据，Arangodb用于存储实时拓扑数据，grafana用于前端页面展示。

![](./png/system_integration.png)

## gala系统安装

Gala提供了集成式部署工具[Gala-Deploy-Tools](./deploy)以便用户快速部署gala-gopher、gala-ops（gala-spider/gala-inference/gala-anteater）组件、kafka/prometheus/arangodb/es/logstash/pyroscope中间件、grafana前端页面展示相关组件，并同时支持离线/在线部署两种模式。
- kafka 用于传输 gala 软件数据
- prometheus 用于存储 gopher metrics数据
- arangodb 用于存储 gala-spider 生成的实时拓扑数据
- elasticsearch/logstash 存储 gala 数据支持 grafana 前端展示
- pyroscope 存储 gopher 火焰图数据
- grafana 展示 gala 前端页面

### 约束限制

1. 当前本工具仅支持如下OS版本：openEuler 20.03 LTS SP1、openEuler 22.03 LTS、openEuler 22.03 LTS SP1、Kylin V10 SP1(x86)、Kylin V10 SP3(x86)
2. 在线部署模式下，本工具运行过程中会从openEuler repo源安装rpm或者从外网下载源码资源，因此内网环境在使用工具前需要提前配置好代理，便于访问外网环境，工具使用结束后建议将代理取消。
4. 离线部署模式下，离线安装包及其依赖包需要从外网下载，因此内网环境需要提前配置好代理，便于访问外网环境，工具使用结束后建议将代理取消。

### 环境准备说明

准备至少两台符合OS版本与架构要求（见约束限制1）的机器（物理机、虚拟机均可）并保证机器间网络可以正常连通（在线部署模式下需要连接外网）。

- 机器A：**生产节点**，即需要监控运维的目标节点，上面一般运行着业务进程（如数据库、redis、Java应用），用于部署观测组件gala-gopher。

  ***注：如果有多台生产节点，则每个节点都需要部署gala-gopher***

- 机器B：**管理节点**，用于部署kafka等中间件以及gala的异常检测、根因定位组件。这些组件的部署相对灵活，可以准备多台管理节点分开部署，只要节点之间网络通即可。

  ***注：管理节点的机器规格建议至少为8U8G***


### 离线部署<a id="deployment-1"></a>

gala组件的运行依赖各个中间件，因此建议按照如下顺序（中间件->gala-gopher/gala-ops->grafana）进行安装部署。

#### 管理节点：部署中间件

当前涉及的中间件包括kafka、prometheus、arangodb、elasticsearch/logstash、pyroscope共6个组件，其中elasticsearch和logstash存在依赖关系，需要绑定部署。

1. 离线安装包下载

离线部署前，需要在可连接外网的机器上下载6个中间件的安装包。本工具提供了[离线资源下载脚本](./deploy/download_offline_res.sh)和[辅助脚本](./deploy/comm.sh)一键全量下载，将两个脚本上传到机器上后执行如下命令完成相关离线资源的下载，下载内容会存放在当前目录的子目录gala_deploy_middleware下。

```xml
sh download_offline_res.sh middleware [os_arch]
```

可选选项：

- os_arch: 指定下载该架构的安装包。未配置该项时，使用当前系统架构。支持架构列表：aarch64 x86_64

注：由于kafka运行依赖java，因此下载kafka安装包时也会同时下载java-1.8.0-openjdk及其依赖包；arangodb组件需要下载容器镜像tar包，因此下载机器上需要安装docker组件。

2. 工具一键部署

将gala_deploy_middleware下的所有文件及[部署脚本](./deploy/deploy.sh)和[辅助脚本](./deploy/comm.sh)上传到目标管理节点机器上，执行如下命令安装、配置、启动kafka/prometheus/elasticsearch/logstash/arangodb/pyroscope服务，**-K/-P/-E/-A/-p选项支持分开使用单独部署对应组件**，-S选项来指定离线安装包所在的目录。

```css
sh deploy.sh middleware -K <部署节点管理IP> -P <prometheus抓取源地址1[,prometheus抓取源地址2,prometheus抓取源地址3,...]> -E <部署节点管理IP> -A -p -S <中间件安装包所在目录>
```

选项详细说明

|       选项       |                                                                                                             参数说明                                                                                                             |               是否必配               |
| :--------------: |:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:| :----------------------------------: |
|   -K\|--kafka    |                                                                              使用该选项用于部署kafka服务器，并配置指定的监听IP地址(一般来说是当前管理节点的IP)。当不使用该选项时，不部署kafka服务                                                                              |       需要部署kafka服务时为必        |
| -P\|--prometheus | 使用该选项用于部署prometheus服务器，并配置指定的抓取消息来源（即部署gala-gopher的生产节点）地址列表，每个地址之间用英文逗号分隔，地址后可以跟随“:端口号”来指定抓取端口，当不指定时，使用默认端口8888；地址前可以加上”主机名-“来标识该地址。<br>例如：-P 192.168.0.1,192.168.0.2:18001,vm01-192.168.0.3:18002。当不使用该选项时，不部署prometheus服务 |   需要部署prometheus服务器时为必配   |
|  -A\|--arangodb  |                                                                                       使用该选项用于部署并启动arangodb数据库服务，该服务默认监听全IP，因此无需指定监听IP。                                                                                       |       需要部署arangodb时为必配       |
| -p\|--pyroscope  |                                                                                        使用该选项用于部署并启动pyroscope服务，该服务默认监听全IP，因此无需指定监听IP。                                                                                        |    需要部署pyroscope服务端时必配     |
|  -E\|--elastic   |                                                       使用该选项用于部署elasticsearch、logstash服务，并指定logstash读取消息的elasticsearch服务器地址(一般来说是当前管理节点的IP)。当不使用该选项时，不部署elaticsearch服务                                                        | 需要部署elasticsearch/logstash为必配 |
|   -S\|--srcdir   |                                                                                                   离线部署时使用该选项来指定离线安装包所在的目录                                                                                                    |            离线部署时必配            |

#### 生产节点：部署gala-gopher

本工具提供容器镜像方式下载、安装、部署gala-gopher并支持在k8s集群内以daemonset方式部署。

本工具提供了[离线资源下载脚本](./deploy/download_offline_res.sh)和[辅助脚本](./deploy/comm.sh)一键下载安装资源，将两个脚本上传到机器上后执行命令完成相关离线资源下载。

1. 对应版本gala-gopher容器镜像下载

```xml
sh download_offline_res.sh gopher [os_version] [os_arch]
```
os_version、os_arch 可同时配置（或同时使用默认值）：
- os_version: 指定下载该操作系统版本 gala-gopher容器镜像。未配置该项时，使用当前系统版本。支持版本列表：openEuler-22.03-LTS-SP1 openEuler-22.03-LTS openEuler-20.03-LTS-SP1 kylin-v10-sp1 kylin-v10-sp3

- os_arch: 指定下载该架构 gala-gopher 容器镜像。未配置该项时，使用当前系统架构。支持架构列表：aarch64 x86_64

下载的容器镜像 tar 包等资源存放在 gala_deploy_gopher 目录下，tar包文件名格式为`gala-gopher-[os_arch]:[os_tag].tar`。下载内容如下：

  ```
  gala-gopher-aarch64:22.03-lts-sp1.tar
  daemonset.yaml.tmpl
  ```

2. 工具一键部署

部署前需将gala_deploy_gopher目录下的所有文件及[部署脚本](./deploy/deploy.sh)和[辅助脚本](./deploy/comm.sh)上传到目标节点机器（daemonset方式需要上传到k8s集群的master节点）上，执行如下命令安装、配置、启动gala-gopher服务，-S选项来指定离线安装包所在的目录。

- 容器镜像方式部署（适用于单节点）

```xml
sh deploy.sh gopher -K <kafka服务器地址> -p <pyroscope服务器地址> -S <离线安装包所在目录>
```

- K8S daemonset方式部署（适用于集群）

```
sh deploy.sh gopher -K <kafka服务器地址> -p <pyroscope服务器地址> -S <离线安装包所在目录> --k8s
```

选项详细说明：

|      选项       |                           参数说明                           |    是否必配    |
| :-------------: | :----------------------------------------------------------: | :------------: |
|   -K\|--kafka   | 指定gala-gopher上报采集数据的目标kakfa服务器地址（一般来说是管理节点的IP），当不配置该选项时，kafka服务器地址使用localhost |       否       |
| -p\|--pyroscope | 指定gala-gopher开启火焰图功能后火焰图上传到的pyroscope服务器地址（用于对接前端界面显示）（一般来说是管理节点的IP），当不配置该选项时，pyroscope服务器地址使用localhost |       否       |
|  -S\|--srcdir   | 离线部署时使用该选项来指定gala-gopher以及其依赖包所在的目录  | 离线部署时必配 |
|      --k8s      |        指定以daemonset方式在k8s集群内部署 gala-gopher        |       否       |

#### 管理节点：部署gala-ops

1. gala-ops容器镜像下载

离线部署前，需要在可连接外网的机器上下载gala-ops（gala-anteater/gala-spider/gala-inference）容器镜像tar包。本工具提供了[离线资源下载脚本](./deploy/download_offline_res.sh)和[辅助脚本](./deploy/comm.sh)一键全量下载，将两个脚本上传到机器上后执行如下命令完成相关离线资源下载，下载内容会存放在当前目录的子目录gala_deploy_ops下。

```xml
sh download_offline_res.sh ops [os_arch]
```

可选选项：

- os_arch: 指定下载该架构的容器镜像。未配置该项时，使用当前系统架构。支持架构列表：aarch64 x86_64

2. 工具一键部署

将gala_deploy_ops目录下的所有文件及[部署脚本](./deploy/deploy.sh)和[辅助脚本](./deploy/comm.sh)上传到目标管理节点机器上，执行如下命令安装、配置、启动gala-ops服务，-S选项来指定容器镜像tar包所在的目录。

```shell
sh deploy.sh ops -K <kafka服务器地址> -P <prometheus服务器地址> -A <arangodb地址> -S <gala-ops容器镜像tar包所在目录>
```

选项详细说明：

|       选项       |                                参数说明                                |    是否必配    |
| :--------------: |:------------------------------------------------------------------:| :------------: |
|   -K\|--kafka    |  指定gala-ops读取消息的kakfa服务器地址（一般来说是管理节点的IP），当不配置该选项时，kafka服务器地址使用localhost  |       否       |
| -P\|--prometheus | 指定gala-ops读取消息的prometheus服务器地址（一般来说是管理节点的IP），当不配置该选项时，prometheus服务器地址使用localhost |       否       |
|  -A\|--arangodb  | 指定gala-ops存储关系图数据的的arangodb服务器地址（一般来说是管理节点的IP），当不配置该选项时，arangodb服务器地址使用localhost |       否       |
|   -S\|--srcdir   |                 离线部署时使用该选项来指定gala-ops容器镜像tar包所在的目录                 | 离线部署时必配 |

#### 管理节点：部署grafana

1. grafana容器镜像与依赖python库下载

离线部署前，需要在可连接外网的机器上下载grafana容器镜像。本工具提供了[离线资源下载脚本](./deploy/download_offline_res.sh)和[辅助脚本](./deploy/comm.sh)一键全量下载，将两个脚本上传到机器上后执行如下命令完成相关离线资源下载，下载内容会存放在当前目录的子目录gala_deploy_grafana下：

```xml
sh download_offline_res.sh grafana [os_arch]
```

可选选项：

- os_arch: 指定下载该架构的容器镜像。未配置该项时，使用当前系统架构。支持架构列表：aarch64 x86_64

2. 工具一键部署

将gala_deploy_grafana下的所有文件、[部署脚本](./deploy/deploy.sh)和[辅助脚本](./deploy/comm.sh)上传到目标管理节点，执行如下命令完成部署，grafana会以容器实例方式运行。

```xml
sh deploy.sh grafana -P <Prometheus服务器地址> -p <pyroscope服务器地址> -E <elasticsearch服务器地址> -S <grafana安装包所在目录> --grafana_addr <grafana 前端地址> --grafana_addr_server_port <grafana 地址服务器的端口号>
```

选项详细说明：

|            选项            |                           参数说明                           |    是否必配    |
| :------------------------: | :----------------------------------------------------------: | :------------: |
|      -P\|--prometheus      | 指定grafana中的prometheus数据源地址（一般来说是管理节点的IP），当不配置该选项时，prometheus数据源使用localhost |       否       |
|      -p\|--pyroscope       | 指定grafana中读取火焰图的pyroscope数据源地址（一般来说是管理节点的IP），当不配置该选项时，pyroscope数据源使用localhost |       否       |
|       -E\|--elastic        | 指定grafana中读取异常检测、拓扑图、根因定位结果的elasticsearch数据源地址（一般来说是管理节点的IP）。当不使用该选项时，elasticsearch数据源使用localhost |       否       |
|       --grafana_addr       | 指定 grafana 前端地址，便于外部访问 grafana 前端页面。当不使用该选项时，默认值为“http://localhost:3000” |       否       |
| --grafana_addr_server_port | 部署后的容器内有个 grafana 地址服务器，用于外部通过 http 接口获取 grafana 前端地址。当不使用该选项时，默认值为 3010。例如用户执行 `curl -X GET localhost:3010`,命令返回 grafana 前端地址——“http://localost:3000” |       否       |
|        -S\|--srcdir        |      离线部署时使用该选项来指定grafana安装包所在的目录       | 离线部署时必配 |

### 在线部署<a id="deployment-2"></a>

#### 获取部署脚本

下载单独的[部署脚本](./deploy/deploy.sh)和[辅助脚本](./deploy/comm.sh)无需下载整个工具，可以直接通过如下命令下载到待部署机器上：

```
wget https://gitee.com/openeuler/gala-docs/raw/master/deploy/deploy.sh --no-check-certificate
wget https://gitee.com/openeuler/gala-docs/raw/master/deploy/comm.sh --no-check-certificate
```

#### 管理节点：部署中间件

执行如下命令安装、配置、启动kafka/prometheus/elasticsearch/logstash/arangodb/pyroscope服务，**-K/-P/-E/-A/-p选项支持分开使用单独部署对应组件**，其中-P用于配置prometheus服务端抓取消息的来源地址（即部署gala-gopher的生产节点）列表，每个地址之间用英文逗号分隔；elasticsearch/logstash由于存在依赖关系，通过-E选项统一控制、绑定安装。

```css
sh deploy.sh middleware -K <部署节点管理IP> -P <prometheus抓取源地址1[,prometheus抓取源地址2,prometheus抓取源地址3,...]> -E <部署节点管理IP> -A -p

```

#### 生产节点：部署gala-gopher

通过如下命令来安装、配置、启动gala-gopher服务：

1. 容器镜像方式（适用于单节点）

```xml
sh deploy.sh gopher -K <kafka服务器地址> -p <pyroscope服务器地址>
```

2. K8S daemonset方式部署（适用于集群）

```css
sh deploy.sh gopher -K <kafka服务器地址>  -p <pyroscope服务器地址> --k8s
```

**注：daemonset方式需要在k8s集群的master节点执行**

#### 管理节点：部署gala-ops

gala-ops组件支持rpm、容器镜像两种部署方式，部署时需要指定kafka、prometheus、arangodb服务器地址，当不指定时，这些中间件的地址默认使用localhost。

1. rpm方式（仅支持openEuler 22.03 LTS SP1)

```shell
sh deploy.sh ops -K <kafka服务器地址> -P <prometheus服务器地址> -A <arangodb地址>
```

2. 容器镜像方式：

```css
sh deploy.sh ops -K <kafka服务器地址> -P <prometheus服务器地址> -A <arangodb地址> --docker
```

#### 管理节点：部署grafana

执行如下命令完成前端页面部署，grafana会以容器实例方式运行。

```xml
sh deploy.sh grafana -P <Prometheus服务器地址> -E <es服务器地址>
```



[gala-ops部署演示视频](https://gitee.com/struggling-li-xiaozi/gala-docs/blob/master/demo/5.%20A-Ops%20%E7%BB%84%E4%BB%B6%E9%83%A8%E7%BD%B2.mp4)中以openEuler 22.03 LTS版本为例演示了使用部署工具完成在生成节点上的gala-gopher以及在管理节点上的gala-ops组件部署的过程。

完成上述部署动作后，即可通过浏览器访问“http://[部署节点IP]:3000” 登录grafana来使用A-Ops，登录用户名、密码默认均为admin。[A-Ops 总体介绍视频](https://gitee.com/openeuler/gala-docs/blob/master/demo/0.%20A-Ops%20%E6%80%BB%E4%BD%93%E4%BB%8B%E7%BB%8D.mp4)中结合grafana前端展示页面对A-Ops整体功能进行了演示。

# 项目路线图

A-Ops主要选择了8个主力场景，阶段性的落地相关解决方案。gala-ops遵从其场景规划路线图，定义自身特性落地计划，相关场景路线图及落地特性参考下图：

![](./png/roadmap.png)



# 特性介绍

## 在线应用性能诊断

### 特性背景

在云环境中，应用性能受负载、资源等环境因素影响最大，这类因素无法在实验室中模拟，所以在线定位能力显得尤其重要，应用性能诊断存在两个难点：1）[无法识别应用性能劣化](https://gitee.com/struggling-li-xiaozi/gala-docs#%E7%89%B9%E6%80%A7%E8%83%8C%E6%99%AF)；2）[无法确定问题根因](https://gitee.com/struggling-li-xiaozi/gala-docs#%E7%89%B9%E6%80%A7%E8%83%8C%E6%99%AF)。

- 无法识别应用性能劣化<a id="无法识别应用性能劣化"></a>

  对于CSP厂商，该问题重要性不亚于问题根因定位，因为CSP厂商对外提供的服务都有SLA的承诺，主动识别云服务SLI性能劣化，对CSP厂商而言可以提前发现问题，避免客户投诉，被动运维改为主动运维。

  我们以CSP厂商常见的DCS场景案例，介绍CSP厂商为什么难以发现云服务SLI性能劣化。

  > 分布式缓存服务（Distributed Cache Service，简称DCS）为租户提供在线分布式缓存能力，常见应用包括Redis、Memcached等，通常用于满足用户高并发及快速数据访问的业务诉求，常见使用场景包括电商、视频直播、游戏应用、社交APP等。

  当前CSP厂商常见的DCS SLI性能监控手段有2种：1）拨测方式模拟租户访问；2）DCS应用软件内性能打点；

  - [ ] 拨测方式模拟租户访问

    ![](./png/DCS-1.png)

    拨测获取的DCS的SLI与真实租户访问DCS体验的SLI实际上并不相同，其中差异包括 服务访问的网络路径、访问方式、访问频率均存在差异，这种差异导致该方式存在DCS性能监控失真问题。

  - [ ] DCS应用软件内性能打点

    ![](./png/DCS-2.png)

    在DCS服务应用内（比如Redis）直接性能打点获取应用性能看上去是个不错的选择，但实际情况出乎意料，经常出现租户已经投诉DCS服务SLA未达标，但是应用层监控依然不能发现问题。分析其原因，是因为应用层的性能统计，未覆盖系统层面对应用性能影响的因素，比如TCP丢包/拥塞、网络时延、块设备I/O时延、进度调度时延等。

- 无法确定问题根因<a id="无法确定问题根因"></a>

  依旧以DCS场景为例，所有CSP厂商提供的云服务都需要通过网络供租户访问，网络因素对云服务性能影响至关重要。除了网络因素，对应用影响最大包括I/O时延、调度时延、内存申请时延等。这些问题目前主要依赖OS诊断工具来实现问题的定界/定位。但是OS诊断工具存在一些问题：

  - [ ] 工具碎片化

    OS诊断工具七国八制，新工具层出不穷（BCC、Blktrace、iostat、netstat等），工具的使用依赖运维人员经验的判断在何时、何地、何种方式使用工具，运维效率取决于人员经验。

  - [ ] 线上环境使用受限

    大部分OS诊断工具都无法常驻系统，依赖故障现场抓取诊断数据，面对随机性故障时，诊断工具就无从下手；另外在面临短暂性sys CPU冲高场景时，系统会出现短暂性无法登录、命令无法执行等情况，诊断工具也会无用武之地。除此以外，还有些工具存在需额外提权、安装受限等线上环境使用的问题。

### 解决方案

#### 高保真采集应用性能SLI

Google针对云服务SLI的评估提出VALET方法，从5个维度综合评估应用性能。我们借鉴其思路，从吞吐量（容量）、时延2个角度评估应用性能（其他维度后续也可能会纳入评估范围）。

![](./png/VALET.png)

为了提升通用性（避免语言强相关性、避免应用适配SDK修改等），[gala-gopher](#gala-gopher)提供一种相对通用的应用性能SLI采集方法，我们采取从OS内核TCP视角采集应用性能数据（即理论上该方法适用所有基于TCP的应用）。

- TCP层采集应用时延性能

  时延性能采集的难点在如何降低网络重传、中断时延、调度时延等因素对时延统计带来的误差影响。参考下图，[gala-gopher](#gala-gopher)会内核软中断处会记录业务Request（访问请求[3]）到来的时间戳（TS1），并在系统调用处记录应用读取业务Request的时间戳（TS2），待云服务应用Response时会执行系统调用Write（TS3），Response会产生TCP数据流并且该TCP数据流达到Request请求端后会产生TCP_ACK（TS4）。
  通过上述四个时间戳，我们分别得到：

  应用时延性能SLI：TS4 - TS1 [1]

  应用接收方向时延：TS2 - TS1  [2]

  应用发送方向时延：TS4 - TS3  [2]

  通过该方法，我们可以实现大部分应用时延性能SLI以及处理过程中不同阶段的时延（便于问题定界）。

  ![](./png/tcp-1.png)

  [^1]: openEuler 22.03 SP1版本已发布。
  [^2]: openEuler 23.09版本待发布。
  [^3]: 假设云服务应用在处理访问请求时，单个TCP连接内，是按照先进先出顺序处理。如果这个假设不成立，上述时延采集可能有误差.
  
- TCP层采集应用吞吐量性能

  吞吐量性能采集的难点在于识别短时吞吐量下降，比如有些场景TCP传输数据过程中出现20ms周期性的滑窗不移动现象，导致平常1~3s完成的数据传输，性能劣化至10s以上才能完成。

  举个形象的例子，TCP吞吐量监控犹如高速公路监控，需要持续的监控高速路上单位时间内是否存在公路资源空闲的情况，单位时间越小监控精度越高。

  这种监控的数据观测底噪、精度带来了挑战，这部分能力规划在openEuler 23.09创新版本上线。

  备注：openEuler 22.03 LTS SP1版本gala-gopher采集的应用吞吐量依然来自于应用自身而非OS系统层面。

#### 基础软件low-level分析

根据前面介绍[问题根因](https://gitee.com/struggling-li-xiaozi/gala-docs#%E7%89%B9%E6%80%A7%E8%83%8C%E6%99%AF)的定位离不开OS系统层面的观测，鉴于现有工具的局限性，[gala-gopher](#gala-gopher)定位OS系统后台服务，提供基础软件全方位的观测能力，基于eBPF技术，持续性、低底噪的方式为采集基础软件运行时数据（主要是Metrics类型数据）。所有采集的性能Metrics数据，均会携带应用（即进程/线程）标签，实现以应用视角下钻式观测系统运行状态。

举例：

```
    {
        table_name: "tcp_abn",   --  tcp 异常统计表名
        entity_name: "tcp_link",   -- tcp 对象名
        fields:
        (
            {
                description: "id of process",
                type: "key",
                name: "tgid",   --> tcp所属进程号
            },
            {
                description: "role",
                type: "key",
                name: "role",   --> tcp类型（客户端/服务端）
            },
            {
                description: "client ip",
                type: "key",
                name: "client_ip",  --> tcp client IP
            },
            {
                description: "server ip",
                type: "key",
                name: "server_ip",  --> tcp server IP
            },
            {
                description: "client port",  --> 以下均是tcp五元组其他标签
                type: "key",
                name: "client_port",  
            },
            {
                description: "server port",
                type: "key",
                name: "server_port",
            },
            {
                description: "protocol",
                type: "key",
                name: "protocol",
            },
            {
                description: "comm",
                type: "label",
                name: "comm",    --> tcp所属进程名
            },
            {
                description: "retrans packets",
                type: "gauge",
                name: "retran_packets",  --> 以下均是tcp异常统计Metrics
            },
            {
                description: "drops caused by backlog queue full",
                type: "gauge",
                name: "backlog_drops",
            },
            {
                description: "sock drop counter",
                type: "gauge",
                name: "sk_drops",
            },
            {
                description: "tcp lost counter",
                type: "gauge",
                name: "lost_out",
            },
            {
                description: "tcp sacked out counter",
                type: "gauge",
                name: "sacked_out",
            },
            {
                description: "drops caused by socket filter",
                type: "gauge",
                name: "filter_drops",
            },
            {
                description: "counter of tcp link timeout",
                type: "gauge",
                name: "tmout_count",
            },
            .....
        )
    }
```

数据观测范围包括网络、I/O、内存、调度等，具体可以参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md)。

![](./png/basic-analysis.png)

结合应用性能SLI、基础软件low-level数据观测，建立应用性能大模型，以前者为KPI，后者为特征量，通过gala-ops内相关组件完成线上问题分析，找到对应用性能劣化贡献值最大的特征量（即某个基础软件low-level Metrics）

### 案例演示

数据库与DCS类似，经常也会遇到I/O、网络类因素干扰，造成应用性能波动，下面我们使用openGauss作为演示案例。

[应用性能诊断视频](https://gitee.com/openeuler/gala-docs/blob/master/demo/1.%20A-Ops%20%E6%95%B0%E6%8D%AE%E5%BA%93%E5%9C%BA%E6%99%AF%20-%20%E5%9C%A8%E7%BA%BF%E5%BA%94%E7%94%A8%E6%80%A7%E8%83%BD%E6%8A%96%E5%8A%A8%E8%AF%8A%E6%96%AD.mp4)


## 系统性能诊断

### 特性背景

系统性能诊断主要用于提供给系统维护SRE日常巡检，提供包括网络（TCP）、I/O等性能劣化的诊断能力。适用于随机性问题追溯，比如网络、I/O性能波动、Socket队列溢出、DNS访问失败、系统调用失败、系统调用超时、进程调度超时等等。

支持的系统性能诊断类型参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md#%E7%B3%BB%E7%BB%9F%E7%BA%A7)。

### 解决方案

系统性能诊断分两类：

- 系统错误/资源不足类

  这类问题通常是依赖一些专家经验规则、用户配置阈值来识别系统问题。

  具体支持范围如下：

  - [ ] TCP相关的异常事件：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md#tcp_link)。具体内容包括：tcp OOM、TCP丢包/重传、TCP 0窗口、TCP建链超时等。
  - [ ] Socket相关的异常事件：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md#endpoint)。具体内容包括：侦听队列溢出、Accept队列溢出、Syn队列溢出、主动/被动建链失败、SYNACK重传等。
  - [ ] 进程相关的异常事件：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md#proc)。具体内容包括：系统调用失败、DNS访问失败、iowait超时、BIO错误、调度超时等。
  - [ ] I/O相关的异常事件：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md#block)。具体内容包括：Request超时、Request错误、磁盘空间不足、inode资源不足等

- 系统性能波动类

  这类问题通常不能简单的通过一些规则、阈值来判断是否出现性能波动。所以需要通过一些AI算法来实时检测。gala-ops建立系统性能KPI以及相应的特征量，对采集到的数据进行离线训练+在线学习，实现在线异常检测，具体原理参考[这里](https://gitee.com/struggling-li-xiaozi/gala-docs#%E5%8E%9F%E7%90%86%E5%8F%8A%E6%9C%AF%E8%AF%AD-)。

  系统性能波动类异常事件：参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech_abnormal.md#%E7%B3%BB%E7%BB%9F%E7%BA%A7)。具体包括 TCP建链性能波动、TCP传输时延性能波动、系统I/O时延性能波动、进程I/O时延性能波动、磁盘读写时延性能波动。

### 案例演示

[系统性能诊断视频](https://gitee.com/openeuler/gala-docs/blob/master/demo/2.%20A-Ops%20%E8%99%9A%E6%8B%9F%E5%8C%96%E5%9C%BA%E6%99%AF%20-%20%E5%9C%A8%E7%BA%BF%E7%B3%BB%E7%BB%9F%E6%80%A7%E8%83%BD%E7%93%B6%E9%A2%88%E8%AF%8A%E6%96%AD.mp4)

### 接口介绍

系统性能诊断结果也可以通过kafka topic形式对外通知，诊断结果内标识出具体的观测实体，以及异常原因。

- 样例1：主机对象内block观测实体异常：

  ```
  {
    "Timestamp": 1586960586000000000,		// 异常事件时间戳
    "event_id": "1586xxx_xxxx"			// 异常事件ID
    "Attributes": {
      "entity_id": "xx",					// 发生异常的观测实体ID（集群内唯一）
      "event_id": "1586xxx_xxxx",			// 异常事件ID（同上）
      "event_type": "sys",				// 异常事件类型（sys: 系统异常，app：应用异常）
      "data": [....],     // optional
      "duration": 30,     // optional
      "occurred count": 6,// optional
    },
    "Resource": {
      "metrics": "gala_gopher_block_err_code",	// 产生异常的metrics
    },
    "SeverityText": "WARN",				// 异常级别
    "SeverityNumber": 13,					// 异常级别编号
    "Body": "20200415T072306-0700 WARN Entity(xx)  IO errors occured. (Block %d:%d, COMM %s, PID %u, op: %s, datalen %u, err_code %d, scsi_err %d, scsi_tmout %d)."								// 异常事件描述
  }
  ```

  用户通过kafka订阅到异常事件后，可以表格化管理，以时间段形式呈现管理，如下：

  | 时间              | 异常事件ID   | 观测实体ID | Metrics                    | 描述                                                         |
  | ----------------- | ------------ | ---------- | -------------------------- | ------------------------------------------------------------ |
  | 11:23:54 CST 2022 | 1586xxx_xxxx | xxx_xxxx   | gala_gopher_block_err_code | 20200415T072306-0700 WARN Entity(xx)  IO errors occured. (Block %d:%d, COMM %s, PID %u, op: %s, datalen %u, err_code %d, scsi_err %d, scsi_tmout %d). |



## 系统I/O全栈观测

### 特性背景

分布式存储（包括块存储、对象存储等）是CSP厂商提供的重要服务，常见的分布式存储服务包括EVS、OBS，几乎所有CSP厂商都有这些云服务，同时这些云服务也是其他云服务的存储后端提供者。所以分布式存储的运维效率会决定CSP厂商整个系统的运维效率。

同时，分布式存储的系统构成复杂，软件来源多样性，系统集群化分布式部署，这些都给该场景的运维带来挑战。具体表现在：

- 不同业务团队之间使用不同的诊断工具，工具之间数据缺乏衔接，运维效率低下。
- 缺乏I/O视角集群运行状态的监控平台，集群型I/O故障诊断能力不足。
- 缺乏历史问题追溯能力，随机性故障诊断能力不足。

### 解决方案

从I/O数据流视角实时绘制分布式存储集群拓扑图，全栈I/O视角完成对分布式存储系统的I/O数据流进行观测。

![](./png/io.png)

备注：

1. 鉴于分布式存储系统软件来源多样性，这里使用ceph作为示例讲解，不同软件解决方案有不同的观测点。但是主要思路基本相同。
2. openEuler 22.03 SP1发布的系统I/O全栈主要是针对ceph场景（未采用SPDK加速），其分布式存储场景会在后续update版本持续更新。

### 案例演示

[分布式存储I/O全栈诊断视频](https://gitee.com/openeuler/gala-docs/blob/master/demo/3.%20A-Ops%20%E5%88%86%E5%B8%83%E5%BC%8F%E5%AD%98%E5%82%A8%E5%9C%BA%E6%99%AF%20%E2%80%93%20%E5%9C%A8%E7%BA%BF%E7%B3%BB%E7%BB%9FIO%E8%AF%8A%E6%96%AD.mp4)

## 精细化性能Profiling

### 特性背景

用户在日常运维过程还经常会遇到僵尸进程、内存泄漏、CPU冲高等问题，这些问题现象表现在系统层面，但是问题根因常见在应用层面。为了能够让系统运维SRE快速定界问题范围，gala-ops提供精细化性能Profiling能力，其支持长期、在线采集系统/应用性能数据，可以快速诊断包括CPU冲高、内存泄漏（或持续增长）、系统调用异常、资源不足等问题。

### 解决方案

通过eBPF + 系统perf事件高频采样系统堆栈数据，高度还原故障现场系统运行状态；也可以根据系统资源操作点Hook采样系统堆栈数据，实时还原系统资源使用情况。

覆盖大部分编程语言（包括C/C++、GO、Rust、java等），提供在线、持续的全栈堆栈信息采集能力。

![](./png/stack_trace.png)

- 采样负载评估：以10ms采样一次数据为例，一次采样逻辑的指令预估 1W条（ CPU 10MS 指令数量大概能够执行 1KW条指令），采样指令数量/CPU单位时间执行数量 = 1W/1KW  0.1%，所以采样负载理论上是 0.1%（每核）
- 数据存储评估：采样数据需保存一段时间用于周期性的转换成函数符号。假设采样频率10ms，转换周期1min，那么最少要保留的采样点：1min/10ms * 单次采样数据。即单核大约 6000个采样点。放大评估，单核大约1.2W个采样点。

gala-ops支持使用Grafana图形界面用来帮助客户更好的理解性能Profiling结果，包括火焰图、时间线图两种形式。

### 案例演示

[精细化性能Profiling视频](https://gitee.com/openeuler/gala-docs/blob/master/demo/4.%20A-Ops%20%E6%95%B0%E6%8D%AE%E5%BA%93%E5%9C%BA%E6%99%AF%20-%20%E7%B3%BB%E7%BB%9F%E9%9A%90%E6%82%A3%E8%AF%8A%E6%96%AD%EF%BC%88%E7%81%AB%E7%84%B0%E5%9B%BE%EF%BC%89.mp4)



## K8S Pod全栈可观测及诊断

### 功能描述

GALA项目将全面支持K8S场景故障诊断，提供包括应用drill-down分析、微服务&DB性能可观测、云原生网络监控、云原生性能Profiling、进程性能诊断等特性。

- K8S环境易部署：gala-gopher提供daemonset方式部署，每个Work Node部署一个gala-gopher实例；gala-spider、gala-anteater以容器方式部署至K8S管理Node。

- 应用drill-down分析：提供云原生场景中亚健康问题的故障诊断能力，分钟级完成应用与云平台之间问题定界能力。

  - 全栈监控：提供面向应用的精细化监控能力，覆盖语言运行时（JVM）、GLIBC、系统调用、内核（TCP、I/O、调度等）等跨软件栈观测能力，实时查看系统资源对应用的影响。
  - 全链路监控：提供网络流拓扑（TCP、RPC）、软件部署拓扑信息，基于这些信息构建系统3D 拓扑，精准查看应用依赖的资源范围，快速识别故障半径。
  - GALA 因果型AI：提供可视化根因推导能力，分钟级定界至资源节点。![](./png/k8s-monitor.png)

- 微服务&DB性能可观测：提供非侵入式的微服务、DB访问性能可观测能力，包括：

  - HTTP 1.x访问性能可观测，性能包括吞吐量、时延、错误率等，支持API精细化可观测能力，以及HTTP Trace能力，便于查看异常HTTP请求过程。
  - PGSQL访问性能可观测，性能包括吞吐量、时延、错误率等，支持基于SQL访问精细化观测能力，以及慢SQL Trace能力，便于查看慢SQL的具体SQL语句。

  ![](./png/db-monitor.png)

- 云原生应用性能Profiling：提供非侵入、零修改的跨栈profiling分析工具，并能够对接pyroscope业界通用UI前端。技术特点包括：

  - 底噪低：benchmark测试场景，对应用干扰<2%。
  - 多语言：支持常见开发语言C/C++、Go、Rust、Java.
  - 多实例：支持同时监控多个进程或容器，UI前端可以对比性分析问题原因。
  - 细粒度：支持指定profiling范围，包括进程、容器、Pod。
  - 多维度：提供OnCPU、OffCPU、MemAlloc不同维度的应用性Profiling。

  ![](./png/Pyroscope-UI.png)

- 云原生网络监控：针对K8S场景，提供TCP、Socket、DNS监控能力，具备更精细化网络监控能力。

![](./png/network-monitor.png)

- 进程性能诊断：针对云原生场景的中间件（比如MySQL、Redis等）提供进程级性能问题诊断能力，同时监控[进程性能KPI](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#%E5%9F%BA%E4%BA%8E%E6%B5%81%E7%9A%84%E8%BF%9B%E7%A8%8B%E6%80%A7%E8%83%BD)、进程相关系统层Metrics（比如[I/O](I/O)、[内存](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#%E5%86%85%E5%AD%98)、[TCP](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#tcp%E6%80%A7%E8%83%BD)等），完成进程性能KPI异常检测以及影响该KPI的系统层Metrics（影响进程性能的原因）。

![](./png/performance-diag.PNG)

### 应用场景

![](./png/k8s-deploy.png)

部署方式：gala-gopher提供daemonset方式部署，每个Work Node部署一个gala-gopher实例；gala-spider、gala-anteater以容器方式部署至K8S管理Node。

相关使用方式参考如下：

[gala-gopher daemonset部署介绍](https://gitee.com/openeuler/gala-docs/blob/master/demo/k8s%E7%8E%AF%E5%A2%83daemonset%E9%85%8D%E7%BD%AE%E5%8F%8A%E9%83%A8%E7%BD%B2.wmv)

[REST配置介绍](https://gitee.com/openeuler/gala-docs/blob/master/demo/Rest%20API%E9%85%8D%E7%BD%AE%E5%8F%8A%E4%BD%BF%E7%94%A8.wmv)



# 待发布特性

1. 系统隐患巡检
2. 线程性能Profiling（解决线程死锁、I/O瓶颈等疑难问题）

# Q&A

## 如何快速使用

系统如何快捷部署？

社区提供两类快捷部署方式，1）[在线部署](https://gitee.com/struggling-li-xiaozi/gala-docs#%E5%9C%A8%E7%BA%BF%E9%83%A8%E7%BD%B2)； 2）[离线部署](https://gitee.com/struggling-li-xiaozi/gala-docs#%E7%A6%BB%E7%BA%BF%E9%83%A8%E7%BD%B2)；前者要求用户安装环境可以访问openEuler社区；后者可以要求用户下载软件后，将其拷贝至安装环境；

## 版本配套

1. gala-gopher支持哪些OS版本？

   - openEuler 22.03 SP1及其之后的LTS版本是正式推荐客户使用的版本；
   - openEuler 22.03 SP1之前的LTS版本也将获得openEuler社区技术支撑，但社区不推荐客户使用；
   - openEuler 系列的商用OS（比如麒麟V10）也将获得openEuler社区技术支撑，但是商用OS厂商暂未对gala-gopher提供商用维保；
   - 非openEuler系列的OS（比如SUSE 12，CentOS等）技术上可以安装、部署gala-gopher，但原则上获取不到社区技术支撑；

2. gala-gopher的观测能力支持哪些内核版本？

   | 内核版本 | 观测能力范围                                                 |
   | -------- | ------------------------------------------------------------ |
   | 4.12     | 在线性能火焰图、TCP、I/O、应用L7层流量、进程、DNS            |
   | 4.18     | 在线性能火焰图、TCP、I/O、应用L7层流量、进程、DNS、Redis（Server侧）时延性能、PG DB（Server侧）时延性能、openGauss（Server侧）时延性能 |
   | 4.19     | 在线性能火焰图、TCP、I/O、应用L7层流量、进程、DNS、Redis（Server侧）时延性能、PG DB（Server侧）时延性能、openGauss（Server侧）时延性能 |
   | 5.10     | 在线性能火焰图、TCP、I/O、应用L7层流量、进程、DNS、Redis（Server侧）时延性能、PG DB（Server侧）时延性能、openGauss（Server侧）时延性能 |

3. gala-gopher是否支持跨内核版本兼容？

   - 在openEuler 22.03 SP1版本中，gala-gopher不支持跨内核版本兼容；
   - 规划在openEuler 22.03 SP3版本中，gala-gopher将支持跨Release版本兼容；（即5.10内核版本范围内，不同release版本可以使用同一个gala-gopher组件）；
   - 规划在openEuler 23.03 SP1版本中，gala-gopher将在低内核版本（4.18/4.19）支持跨Release版本兼容性；
   - 规划在24年，gala-gopher将支持跨内核大版本的兼容性（比如5.10/4.19两个内核版本可以使用相同的gala-gopher软件版本）；

## 性能测试

1. gala-gopher底噪数据？

   测试条件：

   - 硬件环境：x86_64架构，16核，8G内存，虚拟机
   - 软件环境：openEuler-22.03-LTS操作系统，5.10.0内核
   - 应用观测范围：共计观测8个进程，其中包括：
     - 带背景流量（1000records/sec）的 kafka 服务器和benchmark客户端。
     - 带背景流量（3万requests/sec）的 redis 服务器和benchmark客户端。

   测试结果如下：

   | 测试项                                                   | 单核cpu占用率（每隔5s上报一次） |
   | -------------------------------------------------------- | ------------------------------- |
   | 单独启动 systeminfo 探针                                 | 平均1.5%                        |
   | 单独启动 proc 探针                                       | 平均<1%                         |
   | 单独启动 tcp 探针                                        | 平均<1%                         |
   | 单独启动 io 探针                                         | 平均2%                          |
   | 单独启动 endpoint 探针                                   | 平均<1%                         |
   | 单独启动 jvm 探针                                        | 平均2%                          |
   | 单独启动 stackprobe 探针                                 | 平均<1%，最高到1.5%（间隔30s）  |
   | 启动 systeminfo,proc,tcp,io,endpoint,jvm,stackprobe 探针 | 平均5%                          |

2. gala系统需要多少资源？

   | 组件              | 部署位置 | 资源要求                                     |
   | ----------------- | -------- | -------------------------------------------- |
   | gala-gopher       | 生产节点 | 0.2 core，100M内存（只开启TCP，I/O采集能力） |
   | gala-spider       | 管理节点 |                                              |
   | gala-anteater     | 管理节点 |                                              |
   | prometheus        | 管理节点 |                                              |
   | Grafana           | 管理节点 |                                              |
   | kafka             | 管理节点 |                                              |
   | elasticsearch     | 管理节点 |                                              |
   | logstash          | 管理节点 |                                              |
   | arangodb          | 管理节点 | 约束：只支持X86架构                          |
   | pyroscope（可选） | 管理节点 |                                              |

## 支持语言&协议范围

| 协议       | 支持                                                         |
| ---------- | ------------------------------------------------------------ |
| HTTP1.X    | 语言：C/C++、Java、Go（TO BE）、Rust（TO BE）；<br />加密库：openSSL、JSSE、GoSSL（TO BE）、Rustls（TO BE） |
| PostgreSQL | 语言：C/C++、Java、Go（TO BE）、Rust（TO BE）；<br />加密库：openSSL、JSSE、GoSSL（TO BE）、Rustls（TO BE） |
| MySQL      | 语言：C/C++、Java、Go（TO BE）、Rust（TO BE）；<br />加密库：openSSL、JSSE、GoSSL（TO BE）、Rustls（TO BE） |
| Dubbo      | 语言：C/C++、Java、Go（TO BE）、Rust（TO BE）；<br />加密库：openSSL、JSSE、GoSSL（TO BE）、Rustls（TO BE） |
| Redis      | TO BE                                                        |
| Kafka      | TO BE                                                        |
| HTTP 2.0   | TO BE                                                        |
| MongoDB    | TO BE                                                        |

## 支持虚机&容器&K8S环境

1. 支持多种容器运行时：包括containerd、docker、isula三种容器运行时；可以针对这三种容器运行时场景的容器实例监控。

2. gala-gopher是否支持K8S部署、监管？

   gala-gopher支持在K8S环境中以daemonset形式部署，相关的daemonset yaml、容器镜像Dockerfile、容器启动入口脚本请参考：

   https://gitee.com/openeuler/gala-gopher/blob/dev/k8s/daemonset.yaml.tmpl
   https://gitee.com/openeuler/gala-gopher/blob/dev/build/Dockerfile_2003_sp1_x86_64
   https://gitee.com/openeuler/gala-gopher/blob/dev/build/Dockerfile_2003_sp1_aarch64
   https://gitee.com/openeuler/gala-gopher/blob/dev/build/entrypoint.sh

3. 采集的数据、上报的事件均会携带系统上下文标签具体如下：

   **Node标签**：System ID（集群内唯一），管理IP。

   **Device标签**：Device Name（比如网卡、磁盘）。

   **进程标签**：进程ID、进程名、cmdline。

   **网络标签**：TCP clien/server ip、server port、role 标签。

   **容器标签**：容器ID、容器名称。

   **POD标签**：POD ID，POD IP，Pod Name，Pod Namespace标签。

   **用户自定义标签**：采集任务下发时，用户自定义设置的标签。用户自定义标签通过 gala-gopher 提供的动态配置接口进行设置，详情参见链接：[ 配置探针扩展标签](https://gitee.com/algorithmofdish/gala-gopher/blob/dev/config/gala-gopher%E6%94%AF%E6%8C%81%E5%8A%A8%E6%80%81%E9%85%8D%E7%BD%AE%E6%8E%A5%E5%8F%A3%E8%AE%BE%E8%AE%A1_v0.3.md#%E9%85%8D%E7%BD%AE%E6%8E%A2%E9%92%88%E6%89%A9%E5%B1%95%E6%A0%87%E7%AD%BE)。

4. 支持对进程级/容器级 metric 自动扩展容器、POD等公共标签信息。

   gala-gopher 框架侧会根据探针上报的 metric 数据来识别是否为进程级或容器级 metric，并对进程级/容器级 metric 补充容器标签和 POD 标签信息，探针侧不需要单独采集这些额外的标签。

   - **进程级 metric**：如果探针采集的 metric 数据中包括 `tgid` 标签，则该 metric 属于进程级 metric。针对进程级 metric，gala-gopher框架会补充容器公共标签（包括容器ID、容器名称）、POD公共标签（包括POD ID，Pod Name，Pod Namespace）。
   - **容器级 metric**：如果探针采集的 metric 数据中包括 `container_id` 标签，则该 metric 属于容器级 metric。针对容器级 metric，gala-gopher框架会补充容器公共标签（包括容器名称）、POD公共标签（包括POD ID，Pod Name，Pod Namespace）。

## java环境的支持程度

全面支持java环境可观测能力，包括支持[java应用性能Profiling](https://gitee.com/openeuler/gala-gopher/tree/dev/#%E5%9C%BA%E6%99%AF1%E6%8C%81%E7%BB%AD%E6%80%A7%E8%83%BDprofiling)、[java加密应用的网络访问](https://gitee.com/openeuler/gala-gopher/tree/dev/#%E5%9C%BA%E6%99%AF2%E5%BE%AE%E6%9C%8D%E5%8A%A1%E8%AE%BF%E9%97%AE%E6%80%A7%E8%83%BD%E7%9B%91%E6%8E%A7)、[JVM数据采集](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#jvm%E7%9B%91%E6%8E%A7)。

## 如何构建系统拓扑

拓扑构建原理：gala-gopher会提供 L4层网络流、负载分担流、L7层网络流、软件部署拓扑等信息，基于这些信息构建系统3D 拓扑。

[1]: 微服务与进程的部署拓扑依赖业务运维系统提供。

![](./png/topo_theory.png)

系统拓扑用途：主要用于gala因果型AI推导根因定位，基于TCP/RPC拓扑完成故障溯源，基于部署拓扑完成面向应用的drill-down的故障根因定位。

- 关键问题1：如何解决网络NAT产生的网络流拓扑无法建立？

  gala-gopher采集的TCP/RPC流拓扑信息（NAT前的IP/Port信息）会根据linux conntrack表信息，进行NAT转换，上送NAT后的网络流拓扑信息。

- 关键问题2：如何建立中间件的网络流拓扑？

  gala-gopher针对Nginx/Haproxy这类网络负载均衡中间件，提供负载分担流监控能力，可以结合TCP信息快速完成负载分担拓扑构建；

  gala-gopher针对kafka这类消息总线式中间件，提供[消息体（topic）流监控能力]()，可以快速构建消息生产者、消费者拓扑。

- 关键问题3：如何建立K8S集群拓扑？

  待补充。

## gala依赖的中间件

| 中间件        | 功能                                                         | 可替换性分析                                                 |
| ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| prometheus    | 时序化存储gala-gopher metrics数据，并对接gala-ops进行数据处理，同时对接grafana页面显示 | 使用拓扑图、异常检测、根因定位功能时必须且不可替换。只需要gala-gopher metrics数据时非必须，可通过修改[gala-gopher配置文件中的metrics输出方式](https://gitee.com/openeuler/gala-gopher/blob/master/doc/conf_introduction.md#gala-gopherconf)为logs替换为从本地目录/var/log/gala-gopher/metrics获取。 |
| kafka         | 存储gala-gopher亚健康巡检、观测对象元数据、异常检测输出、根因定位结果等数据供用户或者gala-ops内部组件订阅获取 | 使用拓扑图、异常检测、根因定位功能时必须且不可替换。</br> 使用gala-gopher亚健康巡检功能时非必须，可通过修改[gala-gopher配置文件中的event输出方式](https://gitee.com/openeuler/gala-gopher/blob/master/doc/conf_introduction.md#gala-gopherconf)为logs替换为从本地目录/var/log/gala-gopher/event获取。 |
| elasticsearch | 存储gala-gopher亚健康巡检、异常检测输出、根因定位结果、拓扑图数据并将其展示到 grafana 前端 | 在grafana页面上展示亚健康巡检、异常检测输出、根因定位结果、拓扑图数据时必须且不可替换 |
| logstash      | 将kafka消息预处理后存储到elasticsearch                       | 在grafana页面上展示亚健康巡检、异常检测输出、根因定位结果、拓扑图数据时必须且不可替换 |
| arangodb      | 存储 gala-spider 生成的实时拓扑数据                          | 使用拓扑图功能时必须且不可替换                               |
| pyroscope     | 时序化存储 gala-gopher火焰图数据，自带的前端页面提供火焰图实时预览、筛选、横向对比等功能，并对接grafana页面显示火焰图。 | 非必须，火焰图文件可直接在本地目录/var/log/gala-gopher/stackstrace下获取 |

## 如何使用性能火焰图

1. 性能火焰图启动命令示例（基本）：

```shell
curl -X PUT http://localhost:9999/flamegraph -d json='{ "cmd": {"probe": ["oncpu"] }, "snoopers": {"proc_name": [{ "comm": "cadvisor"}] }, "state": "running"}'
```

上述命令是一个最简单的火焰图探针启动命令。该探针采用默认参数对cadvisor进程的cpu占用进行采样，可以周期性在本地生成一个svg格式火焰图。生成的火焰图文件打开如下所示，图中可以看到cadvisor进程的go和c调用栈。



![](D:/Code/gala-docs/png/cadvisor_flamegraph_svg.png)



1. 性能火焰图启动命令示例（进阶）：

更加完整的启动命令示例如下，通过手动设置各种参数实现对火焰图探针的定制化配置。flamegraph探针的完整可配置参数参见[探针运行参数](https://gitee.com/openeuler/gala-gopher/blob/dev/config/gala-gopher%E6%94%AF%E6%8C%81%E5%8A%A8%E6%80%81%E9%85%8D%E7%BD%AE%E6%8E%A5%E5%8F%A3%E8%AE%BE%E8%AE%A1_v0.3.md#%E6%8E%A2%E9%92%88%E8%BF%90%E8%A1%8C%E5%8F%82%E6%95%B0)。

```shell
curl -X PUT http://localhost:9999/flamegraph -d json='{ "cmd": {  "check_cmd": "",  "probe": ["oncpu", "offcpu", "mem"] }, "snoopers": {  "proc_name": [{   "comm": "cadvisor",   "cmdline": "",   "debugging_dir": ""  }, {   "comm": "java",   "cmdline": "",   "debugging_dir": ""  }] }, "params": {  "perf_sample_period": 100,  "svg_period": 300,  "svg_dir": "/var/log/gala-gopher/stacktrace",  "flame_dir": "/var/log/gala-gopher/flamegraph",  "pyroscope_server": "localhost:4040",  "multi_instance": 1,  "native_stack": 0 }, "state": "running"}'
```

上述命令可以启动一个火焰图探针，指定以100ms的频率对cadvisor、java进程进行采样，并每隔300s在本地生成三种svg格式火焰图：cpu火焰图，offcpu火焰图，内存火焰图，同时上述火焰图数据通过多进程实例上报到pyroscope服务器。可以实时通过pyroscope查看，也可以在grafana中配置pyroscope数据源，然后在grafana上查看火焰图。



1. 性能火焰图查看方法示例：

通过上述命令启动火焰图后，即可通过不同的过滤选项实时查看火焰图。下图为某java业务的k8s Pod的内存火焰图查看示例。

(1)处可以选择machine_id，查看不同主机上的进程火焰图。

(2)处可以选择进程id，查看不同进程的火焰图。

(3)处可以选择火焰图类型（oncpu/offcpu/mem），查看不同类型的火焰图。

(6)处为进程的标签，格式为[pid]comm。若为k8s容器内进程，则会有[Pod]name，和[Con]name标签，见(4)(5)处。

(7)处可以选择火焰图的时间段。

![](D:/Code/gala-docs/png/kafka_client_mem_fg.png)



1. 使用性能火焰图定位性能问题：

对火焰图的展示形式也可以根据业务需求进行灵活配置。例如云服务灰度发布版本时，环境中同时部署了新老版本的容器。通过同时查看、对比不同版本的容器实例的火焰图，可以快速发现版本间运行差异，迅速定位性能问题。

在下面的实践中，某主机上部署了新旧两个版本的kafka客户端容器，测试发现新版本容器的cpu占用率较老版本略高。

于是在grafana中同时配置这两个容器的火焰图展示，并选中需要定位的时间段。由图中可以明显发现，新版本（右）比老版本（左）增加了对String.format的调用，从而定位到新版本容器是由于代码中增加了序列化操作，从而导致cpu占用率升高。

![](./png/compare_fg.png)

## 如何使用进行网络问题诊断

参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/network_diag.md)

## 如何进行I/O问题诊断

参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/io_diag.md)

## 如何进行JAVA OOM问题诊断

待补充

# 合作厂商

![](./png/partner.png)
