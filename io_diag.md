# IO问题诊断示例

## 场景介绍

![](./png/./io-arch.png)

I/O问题按照场景划分成虚拟化存储、裸机本地存储、GuestOS等几个场景。



## 通过可观测诊断

可观测的诊断过程依赖运维人员对系统、业务熟悉，并具备常见问题的判断能力，gala系统的可观测是辅助运维人员更好的定位、决策整个诊断过程。

其具备问题诊断过程清晰、可回溯，可信度高的优点。下面我们介绍通常的定位过程：

- **问题1**：在虚拟化存储场景中，Qemu进程（VM）之间会共享访问虚拟化存储，现有运维体系中通常只具备磁盘的监控，缺乏VM粒度（实际就是Qemu进程粒度）的监控能力。gala提供VM粒度的I/O访问时延、错误。

| metrics_name  | metrics_type | unit | metrics description                                |
| ------------- | ------------ | ---- | -------------------------------------------------- |
| tgid          | lable        |      | 进程号                                             |
| comm          | lable        |      | 进程名                                             |
| bio_latency   | Gauge        | ns   | I/O operation delay at the BIO layer (unit: us).   |
| bio_err_count | Gauge        |      | Number of I/O operation failures at the BIO layer. |



- **问题2**：系统周期性的有规律的出现I/O性能下降的问题，通常这类问题是一些定时性任务（比如备份日志、压缩数据之类）触发多个进程同时进行写盘操作。gala提供进程粒度的写盘操作观测能力：

| metrics_name | metrics_type | unit | metrics description           |
| ------------ | ------------ | ---- | ----------------------------- |
| tgid         | lable        |      | 进程号                        |
| comm         | lable        |      | 进程名                        |
| container_id | lable        |      | 容器ID                        |
| pod_id       | lable        |      | Pod ID                        |
| ns_fsync     | Gauge        | ns   | 进程系统调用fsync时长，单位ns |



- **问题3**：在GuestOS场景中，通常会遇到存储后端（如果是虚拟化存储可以与问题1结合一起看）性能波动的情况，此时我们无法判断是GuestOS自身引发的问题，还是存储后端的问题。gala系统为我们提供这方面问题定界能力，它提供Block层细粒度的观测，相关Metrics如下：

| metrics_name       | metrics_type | unit | metrics description      |
| ------------------ | ------------ | ---- | ------------------------ |
| major              | lable        |      | 块对象编号               |
| first_minor        | lable        |      | 块对象编号               |
| blk_name           | lable        |      | 块对象名称               |
| disk_name          | lable        |      | 磁盘名称                 |
| latency_req_max    | Gauge        | us   | block层I/O操作时延最大值 |
| latency_driver_max | Gauge        | us   | 驱动层时延最大值         |
| latency_device_max | Gauge        | us   | 设备层时延最大值         |

latency_device_max 代表GuestOS存储后端的执行时延，如果这个数据过大，通常意味着GuestOS的存储后端出现问题，需要继续排查存储后端。

- **问题4**：很多I/O问题是由系统资源泄漏、不足、错误引发的问题现象，gala针对系统资源泄漏、不足、错误提供的全面的巡检能力。包括但不限于：磁盘不足、FD资源泄漏、dentry资源泄漏、磁盘介质报错、inode资源不足等。

![](./png/event.png)



## 通过智能运维诊断

智能运维的诊断过程对运维人员要求较低，对系统、业务的熟悉程度较低，但其存在定位问题类型数量固定、且较少；定位过程不可回溯，可信度较低，最终结果仍需运维人员核实。

- 前置条件1：集群内需部署gala-anteater、gala-spider组件以及相关kafka、logstash、ES等组件；
- 前置条件2：开启异常检测、根因定位能力；



效果展示：以可视化形式呈现故障传播过程，下图是gaussdb场景中，使用chasblade注入磁盘故障，进而影响gaussdb性能。可以直观的看到磁盘上的一些异常事件引发的gaussdb的性能异常。

其原理是：

1. 通过通用软件知识构建系统拓扑图（gala-spider），单例是单个Node的拓扑关系（进程、TCP、磁盘、主机等观测实体之间的关系）。
2. 基于eBPF完成观测实体内可观测数据的采集（gala-gopher）。
3. 基于机器学习算法（gala-anteater）针对观测数据进行异常发现。
4. 结合专家规则+PC算法（gala-anteater），在多个异常事件之间完成可视化推导（结合前面的拓扑图）。

![](./png/io-demo.png)

