import os
from http.server import HTTPServer, BaseHTTPRequestHandler

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        addr = os.getenv('grafana_server_addr')
        if addr is None:
            addr = 'http://127.0.0.1:3000' # default grafana server addr
        self.send_response(200)
        self.end_headers()
        self.wfile.write(str.encode(addr))


if __name__ == "__main__":
    http_port = os.getenv('grafana_addr_server_port')
    if http_port is None:
        http_port = 3010 # default http port
    
    httpd = HTTPServer(('0.0.0.0', int(http_port)), SimpleHTTPRequestHandler)
    httpd.serve_forever()