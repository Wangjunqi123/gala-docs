# 网络问题诊断示例

## 场景介绍

![](./png/./cloudnative-arch.png)

为了方便介绍，我们假设上图是企业云原生架构（实际远比其复杂），但为了能够更简单说明gala的网络问题定位能力，我们以该图作为案例背景。

- 前置条件1：如上图所示，每个生产Node都需部署gala-gopher；
- 前置条件2：根据生产环境，不同Node开启不同的采集能力，比如前后端服务开启L7层探针，LB开启TCP探针，存在DNS访问的Node都开启proc探针；



## 通过可观测诊断

可观测的诊断过程依赖运维人员对系统、业务熟悉，并具备常见问题的判断能力，gala系统的可观测是辅助运维人员更好的定位、决策整个诊断过程。

其具备问题诊断过程清晰、可回溯，可信度高的优点。下面我们介绍通常的定位过程：

- **问题发现**：容器内应用访问吞吐量、时延下降的情况，可以查看Prometheus metrics获取信息如下：

  详细内容参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#%E5%BA%94%E7%94%A8%E5%BE%AE%E6%9C%8D%E5%8A%A1%E8%AE%BF%E9%97%AE%E6%80%A7%E8%83%BD)。

- **可能原因1**：服务之间访问通常需要通过DNS发现对端服务地址，DNS发现过程如果出现问题，会导致服务访问性能下降，可以通过Prometheus Metrics获取[DNS访问性能](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#dns%E8%AE%BF%E9%97%AE%E7%9B%91%E6%8E%A7)。

- **可能原因2**：LB Node发生建链失败的情况（比如超出链接规格），导致服务访问失败率增加，可观测数据参考[这里](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#socket%E7%9B%91%E6%8E%A7-1)。

- **可能原因3**：基础网络发生拥塞、丢包现象，[导致TCP层发生重传现象](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#tcp%E6%80%A7%E8%83%BD)。如果确认TCP层出现这类问题，可以根据TCP五元组继续排查基础网络设施。

- **可能原因4**：某些[应用误触发发送RST报文](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#tcp%E5%BC%82%E5%B8%B8%E7%9B%91%E6%8E%A7)，导致访问链路断开。如果确认是对端误发送RST报文，可以根据TCP五元组，找到发送者（具体某个微服务），继续排查原因。

- **可能原因5**：[TCP出现零窗问题](https://gitee.com/openeuler/gala-docs/blob/master/gopher_tech.md#tcp%E6%80%A7%E8%83%BD)，如果出现这种问题，一般发送者太快，或者接收者太慢。当出现这类问题时，通常伴随着系统CPU冲高这类问题现象。

上述所有Metrics可以通过Grafana查看，如下图查看TCP rtt、drop两个Metrics指标。Grafana界面可以根据自身需要，按需调取查看不同的Metrics。

![](./png/tcp_demo.png)



## 通过智能运维诊断

智能运维的诊断过程对运维人员要求较低，对系统、业务的熟悉程度较低，但其存在定位问题类型数量固定、且较少；定位过程不可回溯，可信度较低，最终结果仍需运维人员核实。

- 前置条件1：集群内需部署gala-anteater组件以及相关kafka、logstash、ES等组件；
- 前置条件2：开启异常检测能力；



效果展示：以报表形式主动上报KPI（比如应用的吞吐量、时延）的故障以及可能的原因（TopN方式排序）

![](./png/anteater-demo.png)

