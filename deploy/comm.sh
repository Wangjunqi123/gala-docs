
OS_ARCH=$(uname -m)
DASHBOARD_LIST=(
"Application Performance.json" "Network Monitor.json" \
"System Performance Monitor.json" \
"JVM Metrics.json" "System Flame.json" \
"AppDetail-AppAccessPanels.json" \
"AppDetail-AppIOPanels.json" \
"AppDetail-AppL7ApiPanels.json" \
"AppDetail-Network.json" \
"ContaninerDetail.json" \
"HostDetail.json" \
"System Flame-export.json" \
"clusterDiagnosisPanels.json")

function docker_pull_image() {
    tag_name="$1"

    if ! docker --version >/dev/null 2>&1 ; then
         echo_warn "Docker cmd not found, try installing docker firstly"
         yum install docker -y
         [ $? -ne 0 ] && echo_err_exit "Error: fail to install docker"
    fi

    docker pull ${DOCKER_HUB_TAG_PREFIX}/"${tag_name}" 2>gopher_deploy_err.log
    if [ $? -ne 0 ] ; then
        if cat gopher_deploy_err.log | grep -q "unknown authority" ; then
            echo_warn "Add hub.oepgs.net to insecure-registries in /etc/docker/daemon.json and restart docker, then retry deploying"
        fi
        rm -rf gopher_deploy_err.log
        echo_err_exit "Error: failed to pull docker image: ${DOCKER_HUB_TAG_PREFIX}/${tag_name}"
    else
        rm -rf gopher_deploy_err.log
    fi
}

function yum_download() {
    rpm="$@"
    repo_path=""

    if echo $REMOTE_REPO_PREFIX | grep -q "openEuler-22.03-LTS-SP1" ; then
        EPOL_REPO=$REMOTE_REPO_PREFIX/EPOL/main/${OS_ARCH}
        EPOL_UPDATE_REPO=$REMOTE_REPO_PREFIX/EPOL/update/main/${OS_ARCH}
    else
        EPOL_REPO=$REMOTE_REPO_PREFIX/EPOL/${OS_ARCH}
        EPOL_UPDATE_REPO=$REMOTE_REPO_PREFIX/EPOL/update/${OS_ARCH}
    fi

    repo_path="--repofrompath=epol_deploy,$EPOL_REPO \
        --repofrompath=epol_update_deploy,$EPOL_UPDATE_REPO \
        --repofrompath=everything_deploy,$REMOTE_REPO_PREFIX/everything/${OS_ARCH} \
        --repofrompath=update_deploy,$REMOTE_REPO_PREFIX/update/${OS_ARCH}"

    echo $repo_path
    yumdownloader --resolve $rpm $repo_path --destdir=${DOWNLOAD_DIR}  --installroot=${DOWNLOAD_DIR} --forcearch=${OS_ARCH} --nogpgcheck -b
    [ $? -ne 0 ] && echo_err_exit "Error: failed to download $rpm, please check repo!"
}


download_grafana_dashboard() {
    download_dir=$1
    for ele in "${DASHBOARD_LIST[@]}"
    do
        echo $ele
        wget "https://gitee.com/openeuler/gala-docs/raw/master/grafana/dashboard/${ele}" \
            -O  "${download_dir}/${ele}" --no-check-certificate
        [ $? -ne 0 ] && echo_err_exit "Failed to download ${ele}"
    done
}

download_gala_web_rpm() {
    download_dir=$1
    wget "https://gitee.com/openeuler/gala-docs/raw/master/grafana/web_rpm/gala-web-v1.0.0-1.${OS_ARCH}.rpm" \
        -O  "${download_dir}/gala-web-v1.0.0-1.${OS_ARCH}.rpm" --no-check-certificate
    [ $? -ne 0 ] && echo_err_exit "Failed to download gala-web-v1.0.0-1.${OS_ARCH}.rpm"
}
